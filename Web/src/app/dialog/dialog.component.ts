import { IModalDialog,IModalDialogButton,IModalDialogOptions } from 'ngx-modal-dialog';
import { ComponentRef,Component } from '@angular/core';
@Component({
  selector: 'bingo-dialog',
  template:`{{content}}`
}) 
export class DialogComponent implements IModalDialog {
    actionButtons: IModalDialogButton[];
    content:string;
   
    constructor() {
      this.actionButtons = [
        { text: 'Close' }
      ];
    }
   
    dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
      this.content=options.data;
    }
  }