import { Component, ViewChild, ElementRef } from '@angular/core';
import { BingoService } from '../Services/bingo.service';
@Component({
  selector: 'bingo-header',
  templateUrl: './header.component.html',
  styleUrls: ['../app.component.css']
})

export class HeaderComponent {
  lastBall = 'Last Ball';
  lastPick: any={};
  last5Picks: any=[];
  pickedList:any =[];
  bingoService:BingoService;
  @ViewChild('box0') myDiv: ElementRef;

  constructor(bingoService: BingoService){
    this.bingoService = bingoService;
    this.bingoService.deletePicks().subscribe(res => {
      this.getCurrentPick();
    });
  }
  getCurrentPick(){
    this.bingoService.getCurrentPick().subscribe(res => {
      this.lastPick = res ;
      console.log(this.lastPick);
      this.getLast5Picks(this.lastPick);
      this.pickedList.push(res);
  });
  }
  getLast5Picks(lp:number){
    this.bingoService.getLast5Picks(lp).subscribe(res=> {
      this.last5Picks = res;
    });
  }
 
}