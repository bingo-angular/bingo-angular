import { Component, Input, ViewContainerRef, ElementRef , Renderer} from '@angular/core';
import { BingoService } from '../Services/bingo.service';
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'bingo-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['../app.component.css']
})
export class GridComponent {
    data: any= {};
    @Input() headerComponent:HeaderComponent;
    pickedList:any=[];
    constructor(private bingoService: BingoService,public renderer: Renderer, public elem: ElementRef) {
      
   }
   ngOnInit() {
    this.pickedList = this.headerComponent.pickedList;
      this.bingoService.getTickets().subscribe(res => {
      this.data = res;
   });
   }
}