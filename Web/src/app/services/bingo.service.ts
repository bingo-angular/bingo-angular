import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { Injectable} from "@angular/core";
@Injectable({
    providedIn: 'root',
  })
export class BingoService{

    private bingourl='https://bingogameapi.azurewebsites.net/api/bingo'
    constructor(private http: Http) {
    }
    getTickets(){
        return this.http.get(this.bingourl +"/getTickets").pipe(
         map((res: Response)=>res.json()));
         
    }
    getCurrentPick(){
        return this.http.get(this.bingourl +"/getCurrentPick").pipe(
            map((res: Response)=>res.json()));
    }
    getLast5Picks(lp:number){
        return this.http.get(this.bingourl +"/getLast5Picks/"+lp).pipe(
            map((res:Response)=>res.json()));
    }
    deletePicks(){
        return this.http.delete(this.bingourl +"/deletePicks/").pipe(
            map((res:Response)=>res.json()));
    }
    claimBingo(numbers:number[]){
        return this.http.post(this.bingourl+"/claimBingo/",numbers).pipe(
            map((res:Response)=>res.json()));
    }
}