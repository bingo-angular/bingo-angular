import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {MatGridListModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from "@angular/material";
import { HeaderComponent } from './header/header.component';
import { GridComponent } from './grid/grid.component';
import {BingoService} from './Services/bingo.service';
import { ModalDialogModule } from 'ngx-modal-dialog';
import {DialogComponent} from "./dialog/dialog.component";
import {FooterComponent} from "./footer/footer.component";
import {BingoComponent} from "./bingo/bingo.component";
@NgModule({
  declarations: [
    HeaderComponent,
    GridComponent,
    DialogComponent,
    FooterComponent,
    BingoComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MatGridListModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ModalDialogModule.forRoot()
  ],
  providers: [BingoService],
  bootstrap: [BingoComponent],
  entryComponents:[DialogComponent]
})
export class AppModule { }
