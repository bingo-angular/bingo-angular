import { Component, Input, ViewContainerRef, ElementRef , Renderer} from '@angular/core';
import {ModalDialogService} from "ngx-modal-dialog";
import {DialogComponent} from "../dialog/dialog.component";
import {GridComponent} from "../grid/grid.component";
import { BingoService } from '../Services/bingo.service';
@Component({
    selector: 'bingo-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['../app.component.css']
  })
export class FooterComponent {
    @Input() gridComponent: GridComponent;
    box0:number[] = [];
    box1:number[] =[];
    box2:number[] = [];
    box3:number[] = [];
    bingoTicket:number[] = [];
    constructor(private bingoService: BingoService,private modalService: ModalDialogService, private viewRef: ViewContainerRef) {
     }
    claimBingo(){
        this.box0 = [];
        this.box1 = [];
        this.box2 = [];
        this.box3 = [];
       let elements = this.gridComponent.elem.nativeElement.querySelectorAll('.picked');
       if(elements.length <25){
         this.openDialog("Invalid Bingo !!", "Please check your tickets again");
         return;
       }
       for (let entry of elements) {
         switch(entry.id.substring(0,4)){
           case "box0":
           this.box0.push(Number(entry.innerText));
           break;
           case "box1":
           this.box1.push(Number(entry.innerText));
           break;
           case "box2":
           this.box2.push(Number(entry.innerText));
           break;
           case "box3":
           this.box3.push(Number(entry.innerText));
           break;
         }
      }
      if(this.box0.length < 25 && this.box1.length < 25 && this.box2.length < 25 && this.box3.length < 25 ){
       this.openDialog("Invalid Bingo !!", "Please check your tickets again");
       return;
     }else{
       this.bingoTicket = this.box0.length === 25 ? this.box0 : this.box1.length === 25 ? this.box1 : this.box2.length === 25 ? this.box2 : this.box3;
       this.bingoService.claimBingo(this.bingoTicket).subscribe(res => {
         this.openDialog(res,"Congragulations !! You won the Game");
      });
     }
   }
   
     openDialog(title:string,content:string) {
       this.modalService.openDialog(this.viewRef, {
         title: title,childComponent:DialogComponent,data:content
       });
     }
}