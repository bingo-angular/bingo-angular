﻿using BingoModels;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Service
{
    public class BingoService
    {
        public Pick GetCurrentPick()
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                return ctx.Picks.OrderByDescending(x => x.DateCreated).FirstOrDefault();
            }

        }

        public void SaveCurrentPick(Pick p)
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                ctx.Picks.Add(p);

                ctx.SaveChanges();
            }

        }

        public List<Pick> GetLast5Picks(int currentPick)
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                return ctx.Picks.Where(x => x.CurrentPick != currentPick).OrderByDescending(x => x.DateCreated).Take(5).ToList();
            }
        }
        public List<Pick> GetPicks()
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                return ctx.Picks.ToList();
            }
        }

        public void DeletePicks()
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                ctx.Picks.RemoveRange(ctx.Picks);
                ctx.SaveChanges();
            }
        }

        public bool IsValidBingo(List<int> numbers)
        {
            using (BingoEntities ctx = new BingoEntities())
            {
                return !numbers.Except(ctx.Picks.Select(x => x.CurrentPick).ToList()).Any();
            }
        }
    }

       
  }

