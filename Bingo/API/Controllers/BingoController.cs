﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using Service;
using BingoModels;
using System.Web.Http.Cors;

namespace API.Controllers
{
   // [EnableCors("AllowAllHeaders")]
    public class BingoController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        [Route("api/bingo/getCurrentPick")]
        public IHttpActionResult GetCurrentPick()
        {
            Random r = new Random();
            BingoService bs = new BingoService();
            var picks = bs.GetPicks().ToList();
            var exclude = picks.Select(x => x.CurrentPick).ToList();
            if (exclude.Count < 100)
            {
                var range = Enumerable.Range(1, 100).Where(i => !exclude.Contains(i));
                Pick p = new Pick();
                p.CurrentPick = range.ElementAt(r.Next(0, 100 - exclude.Count));
                p.DateCreated = DateTime.Now;
                bs.SaveCurrentPick(p);
                return Ok(p.CurrentPick);
            }
            return Ok(picks.OrderByDescending(x => x.DateCreated).Select(x => x.CurrentPick).FirstOrDefault());
        }

        [Route("api/bingo/getLast5Picks/{currentPick}")]
        public IHttpActionResult GetLast5Picks(int currentPick)
        {
            BingoService bs = new BingoService();
            return Ok(bs.GetLast5Picks(currentPick));
        }
        [Route("api/bingo/getTickets")]
        public IHttpActionResult GetTickets()
        {
            Tickets lt = new Tickets();
            lt.tickets = new List<Ticket>();
            Random rnd = new Random();
           for (int i=0; i<=3; i++)
            {
                var row = Enumerable.Range(1, 100).OrderBy(r => rnd.Next()).Take(25).ToArray().Select(x => new TicketDetail() { number = x , frontEndID = "box"+i+"-"+x }).ToArray().Split(5).ToList();
                var a = new Ticket() { ticket = row};
                lt.tickets.Add(a);
            }
            return Ok(lt);
        }

        [Route("api/bingo/deletePicks")]
        public IHttpActionResult DeletePicks()
        {
           BingoService bs = new BingoService();
           bs.DeletePicks();
           return Ok("done");
        }

        [Route("api/bingo/claimBingo")]
        public IHttpActionResult ClaimBingo(List<int> numbers)
        {
            if(numbers.Count() != 25)
            {
                return Ok("Invalid Bingo !!");
            }
            BingoService bs = new BingoService();
            return Ok(bs.IsValidBingo(numbers) ? "Valid Bingo !!" : "Invalid Bingo !!");
        }

    }
}