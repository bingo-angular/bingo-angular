﻿CREATE TABLE [dbo].[Picks]
(
	[Id] INT NOT NULL PRIMARY KEY identity, 
    [CurrentPick] INT NOT NULL, 
    [DateCreated] DATETIME NOT NULL DEFAULT GetDate() 
)
