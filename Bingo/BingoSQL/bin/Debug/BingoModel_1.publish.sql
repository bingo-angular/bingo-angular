﻿/*
Deployment script for Bingo

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Bingo"
:setvar DefaultFilePrefix "Bingo"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
The column [dbo].[Picks].[CurrentPick] on table [dbo].[Picks] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue you must either: add a default value to the column, mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/

IF EXISTS (select top 1 1 from [dbo].[Picks])
    RAISERROR (N'Rows were detected. The schema update is terminating because data loss might occur.', 16, 127) WITH NOWAIT

GO
PRINT N'Rename refactoring operation with key 0477ff3d-6029-4b75-920e-130b313fa0df is skipped, element [dbo].[Picks].[currentPick] (SqlSimpleColumn) will not be renamed to CurrentPick';


GO
PRINT N'Rename refactoring operation with key 8e89f051-6b4c-4eda-af21-064e6344207f is skipped, element [dbo].[Picks].[dateCreated] (SqlSimpleColumn) will not be renamed to DateCreated';


GO
PRINT N'Starting rebuilding table [dbo].[Picks]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Picks] (
    [Id]          INT      IDENTITY (1, 1) NOT NULL,
    [CurrentPick] INT      NOT NULL,
    [DateCreated] DATETIME DEFAULT GetDate() NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Picks])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Picks] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Picks] ([Id])
        SELECT   [Id]
        FROM     [dbo].[Picks]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Picks] OFF;
    END

DROP TABLE [dbo].[Picks];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Picks]', N'Picks';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
-- Refactoring step to update target server with deployed transaction logs

IF OBJECT_ID(N'dbo.__RefactorLog') IS NULL
BEGIN
    CREATE TABLE [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
    EXEC sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
END
GO
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '0477ff3d-6029-4b75-920e-130b313fa0df')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('0477ff3d-6029-4b75-920e-130b313fa0df')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '8e89f051-6b4c-4eda-af21-064e6344207f')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('8e89f051-6b4c-4eda-af21-064e6344207f')

GO

GO
PRINT N'Update complete.';


GO
